class ElementAttributes:
    def __init__(self, element_id: str = None, element_class: str = None, element_name: str = None, element_type: str = None, element_data_type: str = None, element_href: str = None) -> None:
        self.element_id = element_id
        self.element_class = element_class
        self.element_name = element_name
        self.element_type = element_type
        self.element_data_type = element_data_type
        self.element_href = element_href
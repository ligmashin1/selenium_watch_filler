from typing import Union
from selenium.webdriver import Edge, Firefox, Chrome
from selenium.webdriver.chromium.webdriver import ChromiumDriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
import pdb
from selenium.webdriver.support import expected_conditions as EC

class PunchPageInteraction:
    update_button_css_selector = ".edit-info > h6:nth-child(1) > a:nth-child(1)"
    # this is for the cool kids with python3.10
    # def __init__(self, driver: Edge | Firefox | Chrome | ChromiumDriver) -> None:
    def __init__(self, driver: Union[Edge, Firefox , Chrome, ChromiumDriver]) -> None:
        self.driver = driver
        

    def enter_update_page(self) -> None:
        WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, self.update_button_css_selector))).click()


from typing import Union
from selenium.webdriver import Edge, Firefox, Chrome
from selenium.webdriver.chromium.webdriver import ChromiumDriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.remote.webelement import  WebElement

from helpers.attributes import ElementAttributes


class LoginInteraction:
    company_element_attr = ElementAttributes(element_id="login-comp-input",
                                             element_class="form-control keyboardInput compKeyboard", 
                                             element_name="comp", 
                                             element_type="text")
    name_element_attr = ElementAttributes(element_id="login-name-input", 
                                          element_class="form-control keyboardInput nameKeyboard", 
                                          element_name="name", 
                                          element_type="text")
    passwd_element_attr = ElementAttributes(element_id="login-pw-input", 
                                            element_class="form-control keyboardInput pwKeyboard", 
                                            element_name="pw", 
                                            element_type="password")
    enter_element_attr = ElementAttributes(element_class="btn-lg", 
                                            element_type="submit")
    
    # this is for the cool kids with python3.10
    # def __init__(self, driver: Edge | Firefox | Chrome | ChromiumDriver) -> None:
    def __init__(self, driver: Union[Edge, Firefox , Chrome, ChromiumDriver]) -> None:
        self.driver = driver


    def login(self, user_name: str = "", passwd: str = ""):
        company_element: WebElement = WebDriverWait(self.driver,10).until(EC.visibility_of_element_located((By.ID, self.company_element_attr.element_id)))
        name_element: WebElement = WebDriverWait(self.driver,10).until(EC.visibility_of_element_located((By.ID, self.name_element_attr.element_id)))
        passwd_element: WebElement = WebDriverWait(self.driver,10).until(EC.visibility_of_element_located((By.ID, self.passwd_element_attr.element_id)))
        enter_btn: WebElement = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.CLASS_NAME, self.enter_element_attr.element_class)))
        company_element.send_keys("5511")
        name_element.send_keys(user_name)
        passwd_element.send_keys(passwd)
        enter_btn.click()
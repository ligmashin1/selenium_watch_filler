from typing import Union
from selenium.webdriver import Edge, Firefox, Chrome
from selenium.webdriver.chromium.webdriver import ChromiumDriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import StaleElementReferenceException, ElementClickInterceptedException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.remote.webelement import  WebElement
import time
import pdb

from helpers.attributes import ElementAttributes
from helpers.time_updater import TimeUpdatePageInteraction



class UpdaterTableInteraction:

    day_row_attr = ElementAttributes(element_class="btn btn-custom btn-block btn-lg", 
                                            element_data_type="0")
    hours_column = 4
    date_column = 1

    # this is for the cool kids with python3.10
    # def __init__(self, driver: Edge | Firefox | Chrome | ChromiumDriver) -> None:
    def __init__(self, driver: Union[Edge, Firefox , Chrome, ChromiumDriver]) -> None:
        self.driver = driver

    def iterate_over_table(self, indexes_to_run_on: [int], time_updater: "TimeUpdatePageInteraction") -> [WebElement]:
        row_to_retry_in_case_exception = []
        for row_index in indexes_to_run_on:
            try:
                row = WebDriverWait(self.driver, 10).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, "table.table-regular.table-regular-f tbody tr")))[row_index]

                _date = row.find_elements(By.TAG_NAME, 'td')[self.date_column-1].text
                if not _date:
                    break
                hours_element: WebElement = row.find_elements(By.TAG_NAME, 'td')[self.hours_column-1]
                if not hours_element.text:
                    continue
                hours_needed = int(hours_element.text.split(":")[0]) + int(hours_element.text.split(":")[1])/60
                time.sleep(0.5)
                WebDriverWait(self.driver,10).until(EC.element_to_be_clickable(row.find_elements(By.TAG_NAME, 'td')[self.hours_column-1])).click()
                time.sleep(0.5)
                time_updater.fill_in_hours(hours_needed)
                time.sleep(0.5)
                # Continue with your logic here...
            except IndexError:
                raise "something went wrong with the table"
            ## these are "the ui was too slow" exceptions, so im just repeating that row of the table
            except (StaleElementReferenceException, ElementClickInterceptedException) as e:
                row_to_retry_in_case_exception.append(row_index)
        return row_to_retry_in_case_exception


    def fill_in_the_hours(self, time_updater: "TimeUpdatePageInteraction"):
        row_indexes = range(len(WebDriverWait(self.driver, 10).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, "table.table-regular.table-regular-f tbody tr")))))
        row_to_retry_in_case_exception = self.iterate_over_table(row_indexes, time_updater)
        while row_to_retry_in_case_exception:
            row_to_retry_in_case_exception = self.iterate_over_table(row_to_retry_in_case_exception, time_updater)

from typing import Union
from selenium.webdriver import Edge, Firefox, Chrome
from selenium.webdriver.chromium.webdriver import ChromiumDriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.remote.webelement import  WebElement
from math import ceil

from helpers.attributes import ElementAttributes


class TimeUpdatePageInteraction:
    day_name_element_attr = ElementAttributes(element_id="h4",
                                             element_class="portlet-title portlet-title-general")
    start_minutes_element_attr = ElementAttributes(element_id="emm0", 
                                                   element_class="form-control time-input", 
                                                   element_name="emm0", 
                                                   element_type="tel")
    start_hours_element_attr = ElementAttributes(element_id="ehh0", 
                                                   element_class="form-control time-input", 
                                                   element_name="ehh0", 
                                                   element_type="tel")
    end_minutes_element_attr = ElementAttributes(element_id="xmm0", 
                                                   element_class="form-control time-input", 
                                                   element_name="xmm0", 
                                                   element_type="tel")
    end_hours_element_attr = ElementAttributes(element_id="xhh0", 
                                                   element_class="form-control time-input", 
                                                   element_name="xhh0", 
                                                   element_type="tel")
    update_button_element_attr = ElementAttributes(element_class="modal-popup-btn-confirm", 
                                            element_type="button")
    
    # this is for the cool kids with python3.10
    # def __init__(self, driver: Edge | Firefox | Chrome | ChromiumDriver) -> None: 
    def __init__(self, driver: Union[Edge, Firefox , Chrome, ChromiumDriver]) -> None:
        self.driver = driver
    
    def fill_in_hours(self, hours_needed_today: float = 9):
        def enter_time_text_element(element_id: str, keys_to_send: str = ""):
            time_text_element: WebElement = WebDriverWait(self.driver,10).until(EC.visibility_of_element_located((By.ID, element_id)))
            time_text_element.clear()
            time_text_element.send_keys(keys_to_send)

        start_time = 10, 00
        enter_time_text_element(self.start_hours_element_attr.element_id, f"{start_time[0]}")
        enter_time_text_element(self.start_minutes_element_attr.element_id, f"{start_time[1]}")
        enter_time_text_element(self.end_hours_element_attr.element_id, f"{start_time[0] + ceil(hours_needed_today)}")
        enter_time_text_element(self.end_minutes_element_attr.element_id, f"{start_time[1]}")
        update_button: WebElement = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.CLASS_NAME, self.update_button_element_attr.element_class)))
        update_button.click()


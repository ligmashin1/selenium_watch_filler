#!/bin/python3
from selenium.webdriver import Firefox, Chrome, Edge
from selenium.webdriver.chromium.webdriver import ChromiumDriver
from argparse import ArgumentParser

from helpers.login_page import LoginInteraction
from helpers.punch import PunchPageInteraction
from helpers.update_table import UpdaterTableInteraction
from helpers.time_updater import TimeUpdatePageInteraction



def _parse_parameters():
    parser = ArgumentParser(description="credentials arguments")
    parser.add_argument("-n", "--username", action="store", help="your employee number", required=True)
    parser.add_argument("-p", "--password", action="store", help="password to timewatch", required=True)
    parser.add_argument("-b", "--browser", action="store", help="browser to use", required=False, default="Chrome")
    return parser.parse_args()


def main(browser: str, timewatch_username: str, timewatch_password: str):
    # should technically work with firefox/chromium/edge if installed on machine, didnt work on snap/flatpak
    if browser == "Chrome":
        driver = Chrome()
    elif browser == "Firefox":
        driver = Firefox()
    elif browser == "Edge":
        driver = Edge()
    else:
        raise "not yet supported /:"
    driver.get("https://c.timewatch.co.il/punch/punch.php")
    login_page = LoginInteraction(driver)
    login_page.login(timewatch_username, timewatch_password)
    punch_page = PunchPageInteraction(driver)
    table_page = UpdaterTableInteraction(driver)
    update_page = TimeUpdatePageInteraction(driver)
    punch_page.enter_update_page()
    table_page.fill_in_the_hours(update_page)


if __name__ == "__main__":
    args = _parse_parameters()
    main(browser=args.browser, timewatch_username=args.username, timewatch_password=args.password)